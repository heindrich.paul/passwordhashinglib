package com.paul;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordHashingEngineTest
{

   @Test
   public void testHashPassword()
   {
      String pw = "Pass@word1";
      String actual = PasswordHashingEngine.hashPassword(pw);
      assertEquals("2734359a0d1d08e11ef3f13040f0cf2822870deb0c726b1acce33489d15e69ad", actual);
   }

}

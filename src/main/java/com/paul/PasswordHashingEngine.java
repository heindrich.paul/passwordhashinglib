package com.paul;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class PasswordHashingEngine
{

   public static String hashPassword(String password)
   {
      password = saltPassword(password);
      for (int i = 0; i < 5000; i++)
      {
         password = calculateSHA256(password);
      }
      return password;
   }

   private static String calculateSHA256(String input)
   {
      MessageDigest md = null;
      try
      {
         md = MessageDigest.getInstance("SHA-256");
      }
      catch (NoSuchAlgorithmException e)
      {
      }

      md.update(input.getBytes());
      byte[] mdbytes = md.digest();

      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < mdbytes.length; i++)
      {
         sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
      }

      return sb.toString();
   }

   private static String calculateMD5(String input)
   {
      MessageDigest md = null;
      try
      {
         md = MessageDigest.getInstance("MD5");
      }
      catch (NoSuchAlgorithmException e)
      {
      }

      md.update(input.getBytes());
      byte[] mdbytes = md.digest();

      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < mdbytes.length; i++)
      {
         sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
      }

      return sb.toString();
   }

   private static String saltPassword(String input)
   {
      String[] charactersOfString = new String[input.length()];
      char[] pwd = new char[input.length()];
      String pw = input;
      for (int i = 0; i < input.length(); i++)
      {
         char c = pw.charAt(i);
         charactersOfString[i] = calculateMD5(input.charAt(i) + "");
         long seed = 0;
         for (int k = 0; k < charactersOfString[i].length(); k++)
         {
            seed += (int) charactersOfString[i].charAt(k);
         }
         Random random = new Random(seed);
         byte[] bytes = toBinary(charactersOfString[i] + random.nextLong());
         if (i % 2 == 0)
         {
            for (int j = 0; j < 10; j++)
               c += bytes[j];
         }
         else
         {
            for (int j = bytes.length - 1; j > bytes.length - 11; j--)
               c += bytes[j];
         }
         pwd[i] = c;
      }

      String pwdnew = new String(pwd);

      return pwdnew;
   }

   /**
    * 
    * @param input
    *           The string it needs to convert to a binary byte array
    * @return The binary byte array
    */
   private static byte[] toBinary(String input)
   {
      byte[] bytes = input.getBytes();
      StringBuilder binary = new StringBuilder();
      for (byte b : bytes)
      {
         int val = b;
         for (int i = 0; i < 8; i++)
         {
            binary.append((val & 128) == 0
                  ? 0
                  : 1);
            val <<= 1;
            binary.append(' ');
         }
      }
      String[] bin = binary.toString().split(" ");
      byte[] binbin = new byte[bin.length];
      for (int i = 0; i < bin.length; i++)
      {
         binbin[i] = Byte.parseByte(bin[i]);
      }
      return binbin;
   }

}

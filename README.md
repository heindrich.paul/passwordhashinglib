# Password Hashing Library

This is a library that I wrote to make it easier to hash passwords before saving them to a database or verifying them.


## Authors

* **Heindrich Paul** - [heindrich.paul](https://gitlab.com/heindrich.paul)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
